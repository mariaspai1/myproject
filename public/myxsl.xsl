<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


 
<xsl:template match="/">
  <html>
  <body>


   
<div style="max-width: 600px; margin: 0px auto; x-column-count: 2; x-column-gap: 40px;">
 <xsl:apply-templates select="/tei/text/body/*" />
</div>   


  </body>
  </html>
  
  
</xsl:template>
<xsl:template match="//entry">
    <span style="display: block; line-height: 1.5em; margin: 1.5em 0 1.5em -1em; text-indent: -1em;" xml:space="preserve">      
	<xsl:apply-templates />
	</span>
  </xsl:template>

<!--a default <orth>-->
 <xsl:template match="//orth">
    <span style="font-family: sans-serif;color: red">
      <xsl:apply-templates /> :
    </span>
    <xsl:if test="following-sibling::node()[1][name()='orth']">
      <xsl:text></xsl:text>
      <span style="color: #999999"></span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template>
  
  
  <!--defintion, etymology, note-->
  <xsl:template match="//def | //etym | //note">
    <xsl:apply-templates />
    <xsl:if test="following-sibling::*[1][name()='def' or name()='etym' or name()='note' or (name()='cit' and (@type='translation' or @type='trans')) or name()='quote']">
      <xsl:text></xsl:text>
      <span style="color: blue"></span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template>
  

  <!--various labels-->
  <xsl:template match="//lbl|//usg|//lang">
    <span style="color: blue; font-style: italic;">
      <xsl:apply-templates />|
    </span>
  </xsl:template>

 <xsl:template match="//pos">
    <span style="color: orange; font-style: italic;">
      <xsl:apply-templates />
    |</span>
  </xsl:template>
  
 <!--various references-->
  <xsl:template match="//ref">
    <span style="color: green; font-style: bold;">
      <xsl:apply-templates />
    |</span>
  </xsl:template>



</xsl:stylesheet> 
